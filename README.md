# MXPresentController

category methods for UIViewController

###methods

#####present UIViewController with 4 direction:
	
	[self presentMXViewController:vc direction:MXPD_UP completion:nil]; 
	
#####enable dragBack:

if vc have been presented,just use this code to enable dragBack:
	
	[vc setDragBackEnable:YES];