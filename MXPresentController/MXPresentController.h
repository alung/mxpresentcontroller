//
//  MXPresentController.h
//  MXPresentControllerDemo
//
//  Created by longminxiang on 13-8-12.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    MXPD_UP = 0,
    MXPD_DOWN,
    MXPD_LEFT,
    MXPD_RIGHT
} MXPresentDirection;

@interface UIViewController (MXDragBackController)

@property (nonatomic,assign) BOOL dragBackEnable;

@end

@interface UIViewController (MXPresentController)

- (void)presentMXViewController:(UIViewController *)viewController
                      direction:(MXPresentDirection)direction
                     completion:(void (^)(void))completion;

- (void)presentMXViewController:(UIViewController *)viewController
                     completion:(void (^)(void))completion;

- (void)dismissMXViewcontrollerWithDirction:(MXPresentDirection)direction;
- (void)dismissMXViewController;

@end
