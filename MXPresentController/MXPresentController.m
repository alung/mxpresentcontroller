//
//  MXPresentController.m
//  MXPresentControllerDemo
//
//  Created by longminxiang on 13-8-12.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "MXPresentController.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

#define LDURATION self.view.frame.origin.x / self.view.frame.size.width * 0.5
#define RDURATION (1 - self.view.frame.origin.x / self.view.frame.size.width) * 0.5

#pragma mark
#pragma mark ==== Method hooker ====
@interface UIViewController (MXMethodHooker)

- (void)hookMethod;

@end

@implementation UIViewController (MXMethodHooker)

#pragma mark ==== hooker ====
- (void)hookMethod{
    [self hookSel:@selector(presentViewController:animated:completion:)
           newSel:@selector(presentNViewController:animated:completion:)
           tmpSel:@selector(presentOViewController:animated:completion:)];
    
    [self hookSel:@selector(dismissViewControllerAnimated:completion:)
           newSel:@selector(dismissNViewControllerAnimated:completion:)
           tmpSel:@selector(dismissOViewControllerAnimated:completion:)];
    
    [self hookSel:@selector(dismissModalViewControllerAnimated:)
           newSel:@selector(dismissNModalViewControllerAnimated:)
           tmpSel:@selector(dismissOModalViewControllerAnimated:)];
}

- (void)hookSel:(SEL)sel newSel:(SEL)nSel tmpSel:(SEL)tmpSel{
    IMP oIMP = class_getMethodImplementation([self class], sel);
    IMP nIMP = class_getMethodImplementation([self class], nSel);
    if (oIMP == nIMP) return;
    Method oMethod = class_getInstanceMethod([self class], sel);
    Method nMethod = class_getInstanceMethod([self class], nSel);
    class_replaceMethod([self class], tmpSel, method_getImplementation(oMethod), method_getTypeEncoding(oMethod));
    method_setImplementation(oMethod, method_getImplementation(nMethod));
}

#pragma mark ==== Un Use Method ====
- (void)presentOViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion{
}

- (void)dismissOViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion{
    
}

- (void)dismissOModalViewControllerAnimated:(BOOL)animated{
    
}

@end

#pragma mark
#pragma mark ==== DragBackController ====
@implementation UIViewController (MXDragBackController)

@dynamic dragBackEnable;

static int _isMoving;
static CGPoint _startTouch;
static bool _shouldLoadBG;

#pragma mark ==== setters and getters ====

static const char *dragBackEnableKey = "dragBackEnable";
static const char *bgViewKey = "bgView";
static const char *panGestureKey = "panGesture";

- (BOOL)dragBackEnable{
    return [objc_getAssociatedObject(self, dragBackEnableKey) boolValue];
}

- (UIView *)bgView{
    return objc_getAssociatedObject(self, bgViewKey);
}

- (void)setBgView:(UIView *)bgView{
    objc_setAssociatedObject(self, bgViewKey, bgView, OBJC_ASSOCIATION_ASSIGN);
}

- (UIPanGestureRecognizer *)panGesture{
    return objc_getAssociatedObject(self, panGestureKey);
}

- (void)setPanGesture:(UIPanGestureRecognizer *)panGesture{
    objc_setAssociatedObject(self, panGestureKey, panGesture, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark ==== Public Method =====

- (void)setDragBackEnable:(BOOL)dragBackEnable{
    if ([self isKindOfClass:[UINavigationController class]]) return;
    objc_setAssociatedObject(self, dragBackEnableKey, [NSNumber numberWithBool:dragBackEnable], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (dragBackEnable) {
        [self hookMethod];
    }
    else{
        [self cleanGesture];
    }
}

#pragma mark ==== Private Method =====

- (void)createBackground{
    if (!_shouldLoadBG) return;
    //背景View
    [self.view.superview insertSubview:self.bgView belowSubview:self.view];
    
    //阴影
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOffset = CGSizeMake(-6,6);
    self.view.layer.shadowOpacity = 0.2;
    self.view.layer.shadowRadius = 5.0;
    CGRect rect = self.view.bounds;
    rect.size.height -= 10;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:rect];
    self.view.layer.shadowPath = path.CGPath;
    
//    //黑色背景
    CGRect frame = self.bgView.frame;
    UIView *alphaView = [[UIView alloc] initWithFrame:frame];
    [alphaView setAlpha:0];
    [alphaView setBackgroundColor:[UIColor blackColor]];
    [[self bgView] addSubview:alphaView];
    [self.view.superview insertSubview:alphaView belowSubview:self.view];
    
    _shouldLoadBG = NO;
}

- (void)cleanBgView{
    if (self.bgView){
        [self.bgView removeFromSuperview];
        self.bgView = nil;
    }
}

- (void)createGesture{
    [self cleanGesture];
    self.panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(actionWithPaningGesture:)];
    [self.panGesture delaysTouchesBegan];
    [self.view addGestureRecognizer:self.panGesture];
}

- (void)cleanGesture{
    if (self.panGesture){
        [self.view removeGestureRecognizer:self.panGesture];
        self.panGesture = nil;
    }
}

- (void)moveViewWithframe:(CGRect)frame{
    float d = ABS(frame.origin.y) > 40 ? ABS(frame.origin.y) / frame.size.height : ABS(frame.origin.x) / frame.size.width;
    float scale = d * 0.04 + 0.96;
    self.bgView.transform = CGAffineTransformMakeScale(scale, scale);
    float alpha = 0.85 * (1 - d);
    UIView *alphaView = [self.view.superview viewWithTag:11111];
    [alphaView setAlpha:alpha];
    [self.view setFrame:frame];
}

- (void)actionWithPaningGesture:(UIPanGestureRecognizer *)gesture{
    if (!self.bgView) return;
    UIView *keyView = [[UIApplication sharedApplication] keyWindow];
    CGPoint touchPoint = [gesture locationInView:keyView];
    CGRect frame = self.view.frame;
    if (gesture.state == UIGestureRecognizerStateBegan) {
        _isMoving = YES;
        _startTouch = touchPoint;
    }
    else if (gesture.state == UIGestureRecognizerStateEnded){
        float dur ;
        if (touchPoint.x - _startTouch.x > 50 && [gesture velocityInView:keyView].x > 50){
            frame.origin.x = frame.size.width;
            dur = RDURATION;
        }
        else{
            frame.origin.x = 0;
            dur = LDURATION;
        }
        [UIView animateWithDuration:dur animations:^{
            [self moveViewWithframe:frame];
        } completion:^(BOOL finished) {
            if (frame.origin.x != 0) {
                [self dismissViewControllerAnimated:NO completion:nil];
            }
            _isMoving = NO;
        }];
        
        return;
    }
    if (_isMoving) {
        float x = touchPoint.x - _startTouch.x;
        x = x > frame.size.width ? frame.size.width : x;
        x = x < 0 ? 0 : x;
        frame.origin.x = x;
        [self moveViewWithframe:frame];
    }
}

#pragma mark ==== New Method ====

- (void)presentNViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion{
    viewControllerToPresent.bgView = self.navigationController ? self.navigationController.view : self.view;
    if (viewControllerToPresent.dragBackEnable)
        [viewControllerToPresent createGesture];
    [self presentOViewController:viewControllerToPresent animated:flag completion:^{
        _shouldLoadBG = YES;
        [viewControllerToPresent createBackground];
    }];
}

- (void)dismissNViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion{
    [self dismissOViewControllerAnimated:flag completion:completion];
}

- (void)dismissNModalViewControllerAnimated:(BOOL)animated{
    [self dismissOModalViewControllerAnimated:animated];
}

@end

#pragma mark
#pragma mark ==== PresentController ====
@implementation UIViewController (MXPresentController)

- (void)presentMXViewController:(UIViewController *)viewController
                      direction:(MXPresentDirection)direction
                     completion:(void (^)(void))completion{
    [self hookMethod];
    [self.view setUserInteractionEnabled:NO];
    [self presentViewController:viewController animated:NO completion:nil];
    [viewController animationWithDirection:direction isPresent:YES animated:YES completion:^(BOOL finished) {
        [self.view setUserInteractionEnabled:YES];
        if (completion) completion();
    }];
}

- (void)presentMXViewController:(UIViewController *)viewController
                     completion:(void (^)(void))completion{
    [self presentMXViewController:viewController direction:MXPD_RIGHT completion:completion];
}

- (void)dismissMXViewcontrollerWithDirction:(MXPresentDirection)direction animated:(BOOL)animated{
    if (!self.bgView) return;
    [self animationWithDirection:direction isPresent:NO animated:animated completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}

- (void)dismissMXViewcontrollerWithDirction:(MXPresentDirection)direction{
    [self dismissMXViewcontrollerWithDirction:direction animated:YES];
}

- (void)dismissMXViewController{
    [self dismissMXViewcontrollerWithDirction:MXPD_LEFT];
}

- (void)animationWithDirection:(MXPresentDirection)direction isPresent:(BOOL)isPresent animated:(BOOL)animated completion:(void (^)(BOOL finished))completion{
    CGRect frame = self.view.frame;
    float origin;
    switch (direction) {
        case MXPD_UP:
            origin = frame.origin.y;
            frame.origin.y = isPresent ? frame.size.height : -(frame.size.height + frame.origin.y);
            break;
        case MXPD_DOWN:
            origin = frame.origin.y;
            frame.origin.y = isPresent ? -frame.size.height : frame.size.height + frame.origin.y;
            break;
        case MXPD_LEFT:
            origin = frame.origin.x;
            frame.origin.x = isPresent ? -frame.size.width : frame.size.width;
            break;
        case MXPD_RIGHT:
            origin = frame.origin.x;
            frame.origin.x = isPresent ? frame.size.width : -frame.size.width;
            break;
        default:break;
    }
    if (isPresent) {
        [self.view setFrame:frame];
        if (direction == MXPD_LEFT || direction == MXPD_RIGHT) {
            frame.origin.x = origin;
        }
        else{
            frame.origin.y = origin;
        }
    }
    if (animated) {
        [UIView animateWithDuration:0.35 animations:^{
            [self moveViewWithframe:frame];
        } completion:completion];
    }
    else{
        [self moveViewWithframe:frame];
        if (completion) completion(YES);
    }
}

@end
