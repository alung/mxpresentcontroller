//
//  AppDelegate.h
//  MXPresentControllerDemo
//
//  Created by longminxiang on 13-8-12.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
