//
//  ViewController.m
//  MXPresentControllerDemo
//
//  Created by longminxiang on 13-8-12.
//  Copyright (c) 2013年 longminxiang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 5, 280, 280)];
    label.text = [NSString stringWithFormat:@"%d",rand() % 10];
    label.font = [UIFont systemFontOfSize:250];
    label.textColor = [UIColor grayColor];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    UIButton *button1 = [[UIButton alloc]initWithFrame:CGRectMake(50, 5, 220, 40)];
    [button1 setBackgroundColor:[UIColor grayColor]];
    [button1 setTitle:@"dismiss controller" forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(pressBtn1:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button1];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(50, 240, 220, 50)];
    [button setBackgroundColor:[UIColor grayColor]];
    [button setTitle:@"present a new controller" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(pressBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    UIButton *button2 = [[UIButton alloc]initWithFrame:CGRectMake(50, 300, 220, 50)];
    [button2 setBackgroundColor:[UIColor grayColor]];
    [button2 setTitle:@"Push controller" forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(pressBtn2:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button2];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *textView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 180)];
    textView.font = [UIFont systemFontOfSize:12];
    textView.text = @"MultiLayerNavigation helps you implemntation the interaction -- 'drag to back' in a easy way.\n The only one thing you need to do is replacing your UINavigationController with the MLNavigation Controller or inherit it.";
    textView.numberOfLines = 0;
    
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,300,320,140)];
    scrollView.contentSize = CGSizeMake(640, 200);
    scrollView.userInteractionEnabled = YES;
    [scrollView addSubview:textView];
    //    [self.view addSubview:scrollView];
    
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [loadingView setFrame:CGRectMake(0, 20, 50, 50)];
    [loadingView startAnimating];
    [self.view addSubview:loadingView];
    
    CGRect frame = loadingView.frame;
    frame.origin.y = 400;
    [UIView animateWithDuration:5 delay:0 options:UIViewAnimationOptionRepeat animations:^{
        [loadingView setFrame:frame];
    } completion:nil];
}

- (void)method{
    NSLog(@"11");
}

- (void)pressBtn:(UIButton *)sender{
    ViewController *vc = [ViewController new];
    [vc setDragBackEnable:YES];
    [self presentMXViewController:vc direction:rand() % 4 completion:nil];
}

- (void)pressBtn2:(UIButton *)sender{
    ViewController *vc = [ViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)pressBtn1:(UIButton *)sender{
    [self dismissMXViewcontrollerWithDirction:rand() % 4];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
